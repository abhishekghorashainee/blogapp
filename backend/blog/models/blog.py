from django.db import models
from .user import User
from .base import BaseModel  # Import your BaseModel here

class Category(BaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
# Inherit from BaseModel
class BlogPost(BaseModel):
    title = models.CharField(max_length=200)
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    publication_date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to='blog_thumbnails', null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    tags = models.ManyToManyField('Tag', related_name='blog_posts')
    like_count = models.IntegerField(default=0)
    dislike_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title 

# Comment Model
class Comment(BaseModel):
    comment_text = models.TextField()
    name = models.CharField(max_length=50)
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='comments')
    comment_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Comment by {self.user} on {self.post.title}"
    
# Tag Model
class Tag(BaseModel):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name 