from blog.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status


class RegisterViewTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_register_user(self):
        data = {
            "username": "testuser",
            "email": "testuser@example.com",
            "password": "testpass",
        }
        response = self.client.post("http://localhost:8000/api/auth/register/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.last().email, "testuser@example.com")

    def test_register_user_with_existing_email(self):
        User.objects.create(
            username="testuser", email="testuser@example.com", password="testpass"
        )
        data = {
            "username": "testuser2",
            "email": "testuser@example.com",
            "password": "testpass",
        }
        response = self.client.post("http://localhost:8000/api/auth/register/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

class LoginViewTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", email="testuser@example.com", password="testpass"
        )

    def test_login_user(self):
        data = {"email": "testuser@example.com", "password": "testpass"}
        response = self.client.post("http://localhost:8000/api/auth/login/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("access", response.data)
        self.assertIn("refresh", response.data)

    def test_login_user_with_invalid_credentials(self):
        data = {"email": "testuser@example.com", "password": "wrongpass"}
        response = self.client.post("http://localhost:8000/api/auth/login/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)