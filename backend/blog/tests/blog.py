from blog.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from ..models.blog import BlogPost, Comment


class BlogViewSetTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", email="testuser@example.com", password="testpass"
        )
        self.blog_post = BlogPost.objects.create(
            title="Test Post", content="Test Content", author=self.user
        )
        self.comment = Comment.objects.create(
            comment_text="Test Comment", name="Test Name", post=self.blog_post
        )

    def test_create_blog_post(self):
        self.client.force_authenticate(user=self.user)
        data = {
            "title": "New Post",
            "content": "New Content",
            "category": "Test Category",
            "tags": ["Test Tag 1", "Test Tag 2"]
        }
        response = self.client.post(
            "http://localhost:8000/api/blog/", data, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BlogPost.objects.count(), 2)
        self.assertEqual(BlogPost.objects.last().title, "New Post")
        self.assertEqual(BlogPost.objects.last().category.name, "TEST CATEGORY")
        self.assertEqual(
            list(BlogPost.objects.last().tags.values_list("name", flat=True)),
            ["TEST TAG 1", "TEST TAG 2"],
        )

    def test_like_blog_post(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(
            f"http://localhost:8000/api/blog/{self.blog_post.id}/like/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BlogPost.objects.last().like_count, 1)

    def test_dislike_blog_post(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(
            f"http://localhost:8000/api/blog/{self.blog_post.id}/dislike/",
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BlogPost.objects.last().dislike_count, -1)

    def test_post_comment(self):
        self.client.force_authenticate(user=self.user)
        data = {"comment_text": "New Comment", "name": "New Name"}
        response = self.client.post(
            f"http://localhost:8000/api/blog/{self.blog_post.id}/comment/",
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.blog_post.comments.count(), 2)
        self.assertEqual(self.blog_post.comments.last().comment_text, "New Comment")

    def test_delete_blog_post(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(
            f"http://localhost:8000/api/blog/{self.blog_post.id}/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(BlogPost.objects.count(), 0)
