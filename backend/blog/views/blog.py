from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from ..models.blog import BlogPost
from ..serializers.blog import BlogPostSerializer, CommentSerializer
from blog.pagination import CustomPagination


class BlogViewSet(viewsets.ModelViewSet):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend, SearchFilter]
    pagination_class = CustomPagination
    search_fields = ["title", "content", "author__username", "tags__name"]
    filterset_fields = ["category__name", "author__username"]
    search_fields = [
        "title",
        "content",
        "author__username",
        "tags__name",
        "category__name",
    ]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_serializer_context(self):
        return {"request": self.request}

    @action(detail=True, methods=["post"], url_path="like",  permission_classes = [],)
    def like(self, request, pk=None):
        blog_post = self.get_object()
        blog_post.like_count += 1
        blog_post.save()
        return Response({"message": "Post Liked Successfully"})
    
    @action(detail=True, methods=["post"], url_path="dislike",  permission_classes = [],)
    def dislike(self, request, pk=None):
        blog_post = self.get_object()
        blog_post.dislike_count -= 1
        blog_post.save()
        return Response({"message": "Post Disliked  Successfully"})

    @action(
        detail=True,
        methods=["post"],
        serializer_class=CommentSerializer,
        permission_classes = [],
        url_path="comment",
    )
    def post_comments(self, request, pk=None,):
        blog_post = self.get_object()
        context = self.get_serializer_context()
        serializer = CommentSerializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save(post=blog_post)
        response = BlogPostSerializer(blog_post, context=context)
        return Response(response.data)

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        response.data = {"message": "Blog Post Deleted Successfully"}
        return response