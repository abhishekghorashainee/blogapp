from django.urls import re_path
from blog.views.blog import BlogViewSet
from rest_framework import routers
from blog.views.auth import RegisterView, LoginView

router = routers.DefaultRouter()
router.trailing_slash = '/?'

router.register('blog', BlogViewSet, basename='blog')


urlpatterns = router.urls

urlpatterns += [
    re_path(r'^auth/register/?$', RegisterView.as_view(), name='register'),
    re_path(r'^auth/login/?$', LoginView.as_view(), name='login'),
]