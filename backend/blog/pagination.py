from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

class CustomPagination(PageNumberPagination):
    page_size = 10  # Number of items per page

    def get_paginated_response(self, data):
        return Response({
            'current_page': self.page.number,
            'next_url': self.get_next_link(),
            'prev_url': self.get_previous_link(),
            'results': data  # Your paginated data
        })
