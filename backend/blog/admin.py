from django.contrib import admin
from blog.models import BlogPost,User

admin.site.register(BlogPost)
admin.site.register(User)