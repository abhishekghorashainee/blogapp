from rest_framework import serializers
from blog.models import BlogPost, Tag, Comment, Category


class CommentSerializer(serializers.ModelSerializer):
    post = serializers.HiddenField(
        default=1
    )

    class Meta:
        model = Comment
        fields = [
            "id",
            "name",
            "post",
            "comment_text",
            "comment_date",
        ]


class BlogPostSerializer(serializers.ModelSerializer):
    author_username = serializers.ReadOnlyField(source="author.username")
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    tags = serializers.ListField(child=serializers.CharField(max_length=50), write_only=True)
    tags_names = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True, read_only=True)
    category = serializers.CharField(max_length=100)
    thumbnail = serializers.ImageField(required=False)

    class Meta:
        model = BlogPost
        fields = [
            "id",
            "title",
            "content",
            "author",
            "publication_date",
            "thumbnail",
            "category",
            "tags",
            "tags_names",
            "comments",
            "like_count",
            "dislike_count",
            "author_username",
        ]

    def get_tags_names(self, obj):
        return obj.tags.values_list("name", flat=True)

    def get_category(self, obj):
        return obj.category.name

    def create(self, validated_data):
        tags = validated_data.pop("tags",[])
        category = validated_data.pop("category",[])
        category_obj, _ = Category.objects.get_or_create(name=category.upper())
        validated_data["category"] = category_obj
        blog_post = BlogPost.objects.create(**validated_data)
        for tag in tags:
            tag_obj, created = Tag.objects.get_or_create(name=tag.upper())
            blog_post.tags.add(tag_obj)
        return blog_post

    def update(self, instance, validated_data):
        tags = validated_data.pop("tags",[])
        category = validated_data.pop("category",[])
        if category:
            category_obj, _ = Category.objects.get_or_create(name=category.upper())
            validated_data["category"] = category_obj
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        for tag in tags:
            tag_obj, _ = Tag.objects.get_or_create(name=tag.upper())
            if not instance.tags.filter(name=tag).exists():
                instance.tags.add(tag_obj)
        return instance
