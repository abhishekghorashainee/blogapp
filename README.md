To run backend

- make virtualenv for python (using pyvenv or virtualenvwrapper )
- pip install -r requirements.txt
- python manage.py migrate
- python manage.py runserver

- By default server runs at port 8000
